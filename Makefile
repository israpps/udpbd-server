BIN=udpbd-server
OBJS=main.o
ifeq ($(WINDOWS),1)
	LINKS = -lws2_32 #-llibwsock32.a
endif
udpbd-server: $(OBJS)
	g++ -o $@ $^ $(LINKS)

all: $(BIN)

clean:
	rm -f $(BIN) $(OBJS)

install: $(BIN)
	cp $(BIN) $(PS2DEV)/bin
